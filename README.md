We are a challenge event company dedicated to creating life-changing challenge events for groups and individuals, who wish to raise funds for their chosen charity. We provide charity challenges for those who are seeking adventure, whilst contributing to a worthy cause.

Address: Red Hill House, Hope Street, Chester, Cheshire CH4 8BU, UK

Phone: +44 1244 676454

Website: https://www.globaladventurechallenges.com